package ru.t1consulting.vmironova.tm;

import ru.t1consulting.vmironova.tm.constant.TerminalConst;
import ru.t1consulting.vmironova.tm.constant.ArgumentConst;
import ru.t1consulting.vmironova.tm.model.Command;
import ru.t1consulting.vmironova.tm.util.FormatUtil;

import java.util.Scanner;

public final class Application {

    public static void main(String[] args) {
        if (processArguments(args)) {
            exit();
            return;
        }
        System.out.println("** WELCOME TO TASK-MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            processCommand(command);
        }
    }

    public static boolean processArguments(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.EXIT:
                exit();
            default:
                showErrorCommand();
        }
    }

    public static void processArgument(final String argument) {
        if (argument == null) {
            showErrorArgument();
            return;
        }
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showErrorArgument();
        }
    }

    public static void exit() { System.exit(0); }

    public static void showErrorArgument() { System.out.println("Error! This argument not supported..."); }

    public static void showErrorCommand() { System.out.println("Error! This command not supported..."); }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.7.0");
    }

    public static void showInfo() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = FormatUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = (maxMemoryCheck ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = FormatUtil.formatBytes(totalMemory);
        System.out.println("Total memory: " + totalMemoryFormat);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.formatBytes(usageMemory);
        System.out.println("Usage memory: " + usageMemoryFormat);
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Veronika Mironova");
        System.out.println("E-mail: vmironova@t1-consulting.ru");
        System.out.println("E-mail: mironovavg2@vtb.ru");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.HELP);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
    }

}